<?php

return [
    // 博客版本
    'version' => 'v5.5.3.0',
    'branch' => env('BLOG_BRANCH', 'master')
];
